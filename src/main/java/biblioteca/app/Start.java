package biblioteca.app;

import biblioteca.controller.BibliotecaController;
import biblioteca.model.Carte;
import biblioteca.repository.CartiRepository;
import biblioteca.repository.CartiRepositoryInterface;
import biblioteca.repository.CartiRepositoryMock;
import biblioteca.view.Consola;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

//functionalitati
//i.	 adaugarea unei noi carti (titlu, autori, an aparitie, editura, cuvinte cheie);
//ii.	 cautarea cartilor scrise de un anumit autor (sau parti din numele autorului);
//iii.	 afisarea cartilor din biblioteca care au aparut intr-un anumit an, ordonate alfabetic dupa titlu si autori.



public class Start {
	
	public static void main(String[] args) {
		CartiRepositoryInterface cartiRepositoryMock = new CartiRepository();
		BibliotecaController bibliotecaController = new BibliotecaController(cartiRepositoryMock);
		Consola consola = new Consola(bibliotecaController);
		try {
			consola.executa();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


//		CartiRepositoryInterface cr = new CartiRepository();
//		BibliotecaController bc = new BibliotecaController(cr);
//
//		Carte c = new Carte();
//		bc = new BibliotecaController(cr);
//		c = new Carte();
//
//		List<String> autori = new ArrayList<String>();
//		autori.add("Mateiu Caragiale");
//
//		List<String> cuvinteCheie = new ArrayList<String>();
//		cuvinteCheie.add("mateiu");
//		cuvinteCheie.add("crailor");
//
//		c.setTitlu("Intampinarea crailor");
//		c.setReferenti(autori);
//		c.setAnAparitie("1948");
//		c.setCuvinteCheie(cuvinteCheie);
//
//
//		try {
//			for(Carte ca:bc.getCartiOrdonateDinAnul("1948"))
//				System.out.println(ca);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}


//	try {
//		bc.adaugaCarte(c);
//	} catch (Exception e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//	}
	}

	
}
