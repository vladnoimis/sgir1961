package biblioteca.controller;


import biblioteca.model.Carte;
import biblioteca.repository.CartiRepositoryInterface;
import biblioteca.util.Validator;

import java.util.List;

public class BibliotecaController {

	private CartiRepositoryInterface cartiRepositoryInterface;
	
	public BibliotecaController(CartiRepositoryInterface cartiRepositoryInterface){
		this.cartiRepositoryInterface = cartiRepositoryInterface;
	}
	
	public void adaugaCarte(Carte carte) throws Exception{
		Validator.validateCarte(carte);
		cartiRepositoryInterface.adaugaCarte(carte);
	}
	
	public void modificaCarte(Carte nou, Carte vechi) throws Exception{
		cartiRepositoryInterface.modificaCarte(nou, vechi);
	}
	
	public void stergeCarte(Carte carte) throws Exception{
		cartiRepositoryInterface.stergeCarte(carte);
	}

	public List<Carte> cautaCarte(String autor) throws Exception{
		Validator.isStringOK(autor);
		return cartiRepositoryInterface.cautaCarte(autor);
	}
	
	public List<Carte> getCarti() throws Exception{
		return cartiRepositoryInterface.getCarti();
	}
	
	public List<Carte> getCartiOrdonateDinAnul(String an) throws Exception{
		if(!Validator.isNumber(an))
			throw new Exception("Nu e numar!");
		return cartiRepositoryInterface.getCartiOrdonateDinAnul(an);
	}
	
	
}
