package biblioteca.repository;


import biblioteca.model.Carte;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CartiRepository implements CartiRepositoryInterface {
	
	private String file = "F:\\An3\\VVSS\\01-ProiectBiblioteca\\ProiectBiblioteca\\cartiBD.dat";
	
	public CartiRepository(){
		URL location = CartiRepository.class.getProtectionDomain().getCodeSource().getLocation();
        System.out.println(location.getFile());
	}
	
	@Override
	public void adaugaCarte(Carte carte) {
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(file,true));
			bw.write(carte.toString());
			bw.newLine();
			
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Carte> getCarti() {
		List<Carte> listaCarti = new ArrayList<Carte>();
		BufferedReader bufferedReader = null;
		try {
			bufferedReader = new BufferedReader(new FileReader(file));
			String line = null;
			while((line=bufferedReader.readLine())!=null){
				listaCarti.add(Carte.getCarteFromString(line));
			}
			
			bufferedReader.close();
		} catch (FileNotFoundException exception) {
			exception.printStackTrace();
		} catch (IOException exception) {
			exception.printStackTrace();
		}
		
		return listaCarti;
	}

	@Override
	public void modificaCarte(Carte carteNoua, Carte carteVeche) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stergeCarte(Carte carte) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Carte> cautaCarte(String referent) {
		List<Carte> carti = getCarti();
		List<Carte> cartiGasite = new ArrayList<Carte>();
		int i=0;
		while (i<carti.size()){
			boolean flag = false;
			List<String> listaReferenti = carti.get(i).getCuvinteCheie();
			int j = 0;
			while(j<listaReferenti.size()){
				if(listaReferenti.get(j).toLowerCase().contains(referent.toLowerCase())){
					flag = true;
					break;
				}
				j++;
			}
			if(flag == true){
				cartiGasite.add(carti.get(i));
			}
			i++;
		}
		return cartiGasite;
	}

	@Override
	public List<Carte> getCartiOrdonateDinAnul(String an) {
		List<Carte> listaCarti = getCarti();
		List<Carte> listaAlternativaCarti = new ArrayList<Carte>();
		for(Carte carte:listaCarti){
			if(carte.getAnAparitie().equals(an) == true){
				listaAlternativaCarti.add(carte);
			}
		}
		
		Collections.sort(listaAlternativaCarti,new Comparator<Carte>(){

			@Override
			public int compare(Carte a, Carte b) {
				if(a.getAnAparitie().compareTo(b.getAnAparitie())==0){
					return a.getTitlu().compareTo(b.getTitlu());
				}
				
				return a.getTitlu().compareTo(b.getTitlu());
			}
		
		});
		
		return listaAlternativaCarti;
	}

}
