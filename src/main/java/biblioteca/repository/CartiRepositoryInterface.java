package biblioteca.repository;


import biblioteca.model.Carte;

import java.util.List;

public interface CartiRepositoryInterface {
	void adaugaCarte(Carte carte);
	void modificaCarte(Carte carteNoua, Carte carteVeche);
	void stergeCarte(Carte carte);
	List<Carte> cautaCarte(String referent);
	List<Carte> getCarti();
	List<Carte> getCartiOrdonateDinAnul(String an);
}
