package INT;

import biblioteca.controller.BibliotecaController;
import biblioteca.model.Carte;
import biblioteca.repository.CartiRepository;
import biblioteca.util.Validator;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Name:    ${classname}
 * Effect:
 * Date:    ${Date}
 * Tested:  False
 *
 * @author Simion George-Vlad
 * @version 1.0
 */
public class TestBingBang {
    private CartiRepository cartiRepository;
    private BibliotecaController cartiController;
    private Validator cartiValidator;
    private Carte carte1;
    private Carte carte2;
    private Carte carte3;

    @Before
    public void setUp() {
        cartiValidator = new Validator();
        cartiRepository = new CartiRepository();
        cartiController = new BibliotecaController(cartiRepository);

        List<String> referenti1 = new ArrayList<String>();
        List<String> referenti2 = new ArrayList<String>();
        List<String> cc = new ArrayList<String>();
        referenti1.add("testa");
        referenti1.add("testb");
        referenti1.add("testc");
        referenti1.add("testd");
        referenti2.add("testa");
        referenti2.add("testb");
        referenti2.add("testc");
        cc.add("test");
        carte1 = new Carte();
        carte2 = new Carte();
        carte3 = new Carte();
        carte1.setTitlu("test");
        carte1.setReferenti(referenti1);
        carte1.setAnAparitie("102");
        carte1.setCuvinteCheie(cc);
        carte1.setEditura("test");
        carte2.setTitlu("test");
        carte2.setReferenti(referenti2);
        carte2.setAnAparitie("102");
        carte2.setCuvinteCheie(cc);
        carte2.setEditura("test");
        carte3.setTitlu("test");
        carte3.setReferenti(referenti1);
        carte3.setAnAparitie("102");
        carte3.setCuvinteCheie(cc);
        carte3.setEditura("test");
    }

    @Test
    public void TestModulA() {
        try {
            cartiController.adaugaCarte(carte1);
        } catch (Exception e) {
        }

        List<Carte> biblioteca = cartiRepository.getCarti();
        List<Carte> carti = biblioteca.subList(biblioteca.size() - 1, biblioteca.size());
        assertTrue(carti.get(0).getTitlu().equals(carte1.getTitlu()));
        try {
            cartiValidator.validateCarte(carte1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void TestModulB() {
        try {
            assertTrue(carte1.cautaDupaAutor("testd"));
            assertFalse(carte2.cautaDupaAutor("testd"));
            assertTrue(carte3.cautaDupaAutor("testd"));

        } catch (Exception e) {
        }
    }


    @Test
    public void TestModulC() {
        List<Carte> carti = cartiRepository.getCartiOrdonateDinAnul("1948");
        assertTrue(carti.size() == 3);
    }

    @Test
    public void TestModulABC() {
        try {
            cartiController.adaugaCarte(carte1);
            cartiController.adaugaCarte(carte2);
            cartiController.adaugaCarte(carte3);
        } catch (Exception e) {
        }
        List<Carte> biblioteca = cartiRepository.getCarti();
        List<Carte> carti = biblioteca.subList(biblioteca.size() - 3, biblioteca.size());
        assertTrue(carti.get(0).getTitlu().equals(carte1.getTitlu()));
        assertTrue(carti.get(1).getTitlu().equals(carte2.getTitlu()));
        assertTrue(carti.get(2).getTitlu().equals(carte3.getTitlu()));
        try {
            cartiValidator.validateCarte(carte1);
            cartiValidator.validateCarte(carte2);
            cartiValidator.validateCarte(carte3);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            assertTrue(carte1.cautaDupaAutor("testd"));
            assertFalse(carte2.cautaDupaAutor("testd"));
            assertTrue(carte3.cautaDupaAutor("testd"));

        } catch (Exception e) {
        }


        List<Carte> books = cartiRepository.getCartiOrdonateDinAnul("1948");
        assertTrue(books.size() == 3);
    }
}