package biblioteca;
import biblioteca.controller.BibliotecaController;
import biblioteca.model.Carte;
import biblioteca.repository.CartiRepository;
import org.junit.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
/**
 * Name:    ${classname}
 * Effect:
 * Date:    ${Date}
 * Tested:  False
 *
 * @author Simion George-Vlad
 * @version 1.0
 */
public class TC1 {

        @Test
        public void tc1()   {
            List<String> re = new ArrayList<String>();
            List<String> cc = new ArrayList<String>();
            re.add("test");
            cc.add("test");
            Carte c = new Carte();
            c.setTitlu("test");
            c.setReferenti(re);
            c.setAnAparitie("102");
            c.setCuvinteCheie(cc);
            c.setEditura("test");
            CartiRepository repo = new CartiRepository();
            BibliotecaController ctrl = new BibliotecaController(repo);
            try {
                ctrl.adaugaCarte(c);
            } catch (Exception e) {
                 
            }
        }

    @Test
    public void tc2()   {
        List<String> re = new ArrayList<String>();
        List<String> cc = new ArrayList<String>();
        re.add("test");
        cc.add("test");
        Carte c = new Carte();
        c.setTitlu("");
        c.setReferenti(re);
        c.setAnAparitie("102");
        c.setCuvinteCheie(cc);
        c.setEditura("test");
        CartiRepository repo = new CartiRepository();
        BibliotecaController ctrl = new BibliotecaController(repo);
        try {
            ctrl.adaugaCarte(c);
        } catch (Exception e) {
             
        }
    }

    @Test
    public void tc5()   {
        List<String> re = new ArrayList<String>();
        List<String> cc = new ArrayList<String>();
        re.add("");
        cc.add("test");
        Carte c = new Carte();
        c.setTitlu("1");
        c.setReferenti(re);
        c.setAnAparitie("102");
        c.setCuvinteCheie(cc);
        c.setEditura("test");
        CartiRepository repo = new CartiRepository();
        BibliotecaController ctrl = new BibliotecaController(repo);
        try {
            ctrl.adaugaCarte(c);
        } catch (Exception e) {
             
        }

    }

    @Test
    public void tc6()   {
        List<String> re = null;
        List<String> cc = new ArrayList<String>();
        cc.add("test");
        Carte c = new Carte();
        c.setTitlu("test");
        c.setReferenti(null);
        c.setAnAparitie("102");
        c.setCuvinteCheie(cc);
        c.setEditura("test");
        CartiRepository repo = new CartiRepository();
        BibliotecaController ctrl = new BibliotecaController(repo);
        try {
            ctrl.adaugaCarte(c);
        } catch (Exception e) {
             
        }

    }

    @Test
    public void tc8()   {
        List<String> re = new ArrayList<String>();
        List<String> cc = new ArrayList<String>();
        re.add("test");
        cc.add("test");
        Carte c = new Carte();
        c.setTitlu("test");
        c.setReferenti(re);
        c.setAnAparitie("sgh");
        c.setCuvinteCheie(cc);
        c.setEditura("test");
        CartiRepository repo = new CartiRepository();
        BibliotecaController ctrl = new BibliotecaController(repo);
        try {
            ctrl.adaugaCarte(c);
        } catch (Exception e) {
             
        }
    }

    @Test
    public void tc11()   {
        List<String> re = null;
        List<String> cc = new ArrayList<String>();
        cc.add("test");
        Carte c = new Carte();
        c.setTitlu("test");
        c.setReferenti(re);
        c.setAnAparitie("102");
        c.setCuvinteCheie(cc);
        c.setEditura("test");
        CartiRepository repo = new CartiRepository();
        BibliotecaController ctrl = new BibliotecaController(repo);
        boolean thrown = false;
        try {
            ctrl.adaugaCarte(c);
        } catch (Exception e) {
             
            thrown = true;
        }
        assertTrue(thrown);
    }

    @Test
    public void tc14()   {
        List<String> re = new ArrayList<String>();
        List<String> cc = null;
        re.add("test");
        Carte c = new Carte();
        c.setTitlu("test");
        c.setReferenti(re);
        c.setAnAparitie("102");
        c.setCuvinteCheie(cc);
        c.setEditura("test");
        CartiRepository repo = new CartiRepository();
        BibliotecaController ctrl = new BibliotecaController(repo);
        boolean thrown = false;
        try {
            ctrl.adaugaCarte(c);
        } catch (Exception e) {
             thrown=true;
        }
        assertTrue(thrown);
    }

    @Test
    public void tc18()   {
        List<String> re = new ArrayList<String>();
        List<String> cc = new ArrayList<String>();
        re.add("test");
        cc.add("test");
        Carte c = new Carte();
        c.setTitlu("test");
        c.setReferenti(re);
        c.setAnAparitie("102");
        c.setCuvinteCheie(cc);
        c.setEditura("");
        CartiRepository repo = new CartiRepository();
        BibliotecaController ctrl = new BibliotecaController(repo);
        try {
            ctrl.adaugaCarte(c);
        } catch (Exception e) {
             
        }
    }

    @Test
    public void tc19()   {
        List<String> re = new ArrayList<String>();
        List<String> cc = new ArrayList<String>();
        re.add("test");
        cc.add("test");
        Carte c = new Carte();
        c.setTitlu("test");
        c.setReferenti(re);
        c.setAnAparitie("102");
        c.setCuvinteCheie(cc);
        c.setEditura("123");
        CartiRepository repo = new CartiRepository();
        BibliotecaController ctrl = new BibliotecaController(repo);
        try {
            ctrl.adaugaCarte(c);
        } catch (Exception e) {
             
        }
    }

    @Test
    public void tc4()   {
        List<String> re = new ArrayList<String>();
        List<String> cc = new ArrayList<String>();
        re.add("test");
        cc.add("test");
        Carte c = new Carte();
        c.setTitlu("test");
        c.setReferenti(re);
        c.setAnAparitie("102");
        c.setCuvinteCheie(cc);
        c.setEditura("123");
        CartiRepository repo = new CartiRepository();
        BibliotecaController ctrl = new BibliotecaController(repo);
        try {
            ctrl.adaugaCarte(c);
        } catch (Exception e) {
             
        }
    }

}
