package biblioteca;

import biblioteca.controller.BibliotecaController;
import biblioteca.model.Carte;
import biblioteca.repository.CartiRepository;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Name:    ${classname}
 * Effect:
 * Date:    ${Date}
 * Tested:  False
 *
 * @author Simion George-Vlad
 * @version 1.0
 */
public class TC2 {
    @Test
    public void tc2()   {
        List<String> re = new ArrayList<String>();
        List<String> cc = new ArrayList<String>();
        re.add("test1");
        re.add("test2");
        re.add("test3");
        re.add("test4");
        cc.add("test");
        Carte c = new Carte();
        c.setTitlu("test");
        c.setReferenti(re);
        c.setAnAparitie("102");
        c.setCuvinteCheie(cc);
        c.setEditura("test");
        try {
            assert(c.cautaDupaAutor("test1"));
        } catch (Exception e) {

        }
    }

    @Test
    public void tc3()   {
        List<String> re = new ArrayList<String>();
        List<String> cc = new ArrayList<String>();
        re.add("test1");
        re.add("test2");
        re.add("test3");
        re.add("test4");
        cc.add("test");
        Carte c = new Carte();
        c.setTitlu("test");
        c.setReferenti(re);
        c.setAnAparitie("102");
        c.setCuvinteCheie(cc);
        c.setEditura("test");
        try {
            assert(c.cautaDupaAutor("test4"));
        } catch (Exception e) {

        }
    }


    @Test
    public void tc4()   {
        List<String> re = new ArrayList<String>();
        List<String> cc = new ArrayList<String>();
        re.add("test1");
        re.add("test2");
        re.add("test3");
        re.add("test4");
        cc.add("test");
        Carte c = new Carte();
        c.setTitlu("test");
        c.setReferenti(re);
        c.setAnAparitie("102");
        c.setCuvinteCheie(cc);
        c.setEditura("test");
        try {
            assert(!c.cautaDupaAutor("abc"));
        } catch (Exception e) {

        }
    }
}
