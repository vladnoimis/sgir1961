package biblioteca;

import biblioteca.model.Carte;
import biblioteca.repository.CartiRepository;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Name:    ${classname}
 * Effect:
 * Date:    ${Date}
 * Tested:  False
 *
 * @author Simion George-Vlad
 * @version 1.0
 */
public class TC3 {

    @Test
    public void tcValid()   {
        CartiRepository repo = new CartiRepository();
        List<Carte> carti = repo.getCartiOrdonateDinAnul("1948");
        assertTrue(carti.size()==3);
    }

    @Test
    public void tcInValid()   {
        CartiRepository repo = new CartiRepository();
        List<Carte> carti = repo.getCartiOrdonateDinAnul("xbc");
        assertTrue(carti.size()==0);
    }
}
